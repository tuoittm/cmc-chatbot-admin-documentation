# Mục lục
## [A. Giới thiệu CMC Chatbot Admin](#gioi-thieu)

## [B. Một số khái niệm](#mot-so-khai-niem)
### [1. Ý định và câu mẫu](#mot-so-khai-niem-y-dinh)
### [2. Thực thể](#mot-so-khai-niem-thuc-the)
### [3. Biến](#mot-so-khai-niem-bien)
### [4. Form](#mot-so-khai-niem-form)
### [5. Phản hồi](#mot-so-khai-niem-phan-hoi)
### [6. Kịch bản](#mot-so-khai-niem-kich-ban)

## [C. Xây dựng chatbot trên nền tảng CMC Chatbot Admin](#xay-dung-chatbot)
### [1. Tạo chatbot](#create-chatbot)
### [2. Hiểu ngôn ngữ tự nhiên](#build-nlu)
### [3. Tạo kịch bản](#build-story)
### [4. Tạo phản hồi](#build-response)
### [5. Tạo biến](#build-slot)
### [6. Tạo form](#build-form)
### [7. Liên kết kịch bản](#link-story)
### [8. Rẽ nhánh kịch bản](#branch-story)

## [D. Quản lý chatbot trên nền tảng CMC Chatbot Admin](#quan-ly-chatbot)
### [1. Phân quyền](#phan-quyen)
### [2. Thống kê](#thong-ke)
### [3. Lịch sử](#lich-su)
### [4. Phân tích](#phan-tich)
### [5. Incomming message](#incomming-message)

## [F. Tích hợp chatbot trên các nền tảng](#tich-hop-chatbot)

<div id="gioi-thieu">

# A. Giới thiệu CMC Chatbot Admin

<div id="mot-so-khai-niem">

# B. Một số khái niệm

<div id="mot-so-khai-niem-y-dinh">

## 1. Ý định và câu mẫu
- Ý định là ý muốn của người dùng được thể hiện trong câu nói.
- Câu mẫu là câu mô phỏng ý định của người dùng được sử dụng để huấn luyện chatbot.

Ví dụ 1:

    - Câu mẫu: Tôi muốn đặt bàn cho 5 người vào 8 giờ tối nay.
    - Ý định: đặt bàn

Ví dụ 2:

    - Câu mẫu: Tôi muốn hủy đặt bàn mã số 001A
    - Ý định: hủy đặt bàn

<div id="mot-so-khai-niem-thuc-the">

## 2. Thực thể
- Thực thể là thông tin quan trọng trong câu nói.
- Giá trị của thực thể là từ hoặc cụm từ chứa thông tin của thực thể.

Ví dụ 1:

    - Câu mẫu: Tôi muốn đặt bàn cho 5 người vào 8 giờ tối nay.
    - Thực thể và giá trị của thực thể:
        - số lượng người: 5
        - thời gian: 8 giờ tối nay

Ví dụ 2:

    - Câu mẫu: Tôi muốn hủy đặt bàn mã số 001A
    - Thực thể và giá trị của thực thể:
        - mã đặt bàn: 001A

<div id="mot-so-khai-niem-bien">

## 3. Biến

Biến được sử dụng để lưu trữ thông tin tạm thời trong chatbot, hay nói cách khác, biến chính là bộ nhớ tạm của chatbot. Biến sẽ lưu thông tin hiện tại cho đến khi bạn xóa bộ nhớ hoặc khởi tạo lại biến hoặc ghi đè một thông tin mới, do đó, gọi biến là bộ nhớ tạm của chatbot. Ví dụ biến *thời gian* lưu trữ thông tin về thời gian đặt bàn, biến *số lượng* lưu trữ thông tin về số lượng chỗ ngồi trong một đơn đặt bàn, khi có một đơn đặt bàn mới, giá trị của các biến sẽ thay đổi.

<div id="mot-so-khai-niem-form">

## 4. Form

Form là một dạng biểu mẫu được sử dụng để kiểm tra và thu thập thông tin từ người dùng. Ví dụ để tạo một đơn đặt bàn cần thu thập các thông tin về số lượng người, thời gian đặt bàn. Form sẽ hỗ trợ kiểm tra tính hợp lệ của thông tin và thu thập thông tin còn thiếu bằng cách đặt các câu hỏi cho người dùng. Thông tin thu thập được từ form được lưu vào các [biến](#mot-so-khai-niem-bien).

<div id="mot-so-khai-niem-phan-hoi">

## 5. Phản hồi
Phản hồi là tin nhắn của chatbot gửi cho người dùng

Ví dụ:

    - Tin nhắn của người dùng: "Tôi muốn đặt bàn cho 5 người vào 8 giờ tối nay."
    - Phản hồi của chatbot: "Đặt bàn thành công."

Có 4 loại phản hồi:

[Phản hồi dạng văn bản](#phan-hoi-dang-van-ban)

[Phản hồi dạng nút bấm](#phan-hoi-dang-nut-bam)

[Phản hồi dạng ảnh](#phan-hoi-dang-anh)

[Phản hồi dạng slide ảnh](#phan-hoi-dang-slide-anh)

<div id="phan-hoi-dang-van-ban">

**a. Phản hồi dạng văn bản**

Phản hồi dạng văn bản là mỗi chuỗi các chữ cái, chữ số... Đây là loại phản hồi đơn giản nhất.

Ví dụ:

    - Tin nhắn của người dùng: "Tôi muốn đặt bàn cho 5 người vào 8 giờ tối nay."
    - Phản hồi của chatbot: "Đặt bàn thành công."

Phản hồi trên của chatbot là một loại phản hồi dạng văn bản.

<div id="phan-hoi-dang-nut-bam">

**b. Phản hồi dạng nút bấm**

Phản hồi dạng nút bấm là phản hồi có chứa nút bấm.

Ví dụ:

<img src="images/response_button.png" alt="Phản hồi dạng nút bấm"/>

<div id="phan-hoi-dang-anh">

**c. Phản hồi dạng ảnh**

Phản hồi dạng ảnh là phản hồi có chứa ảnh.

Ví dụ:

<img src="images/response_image.png" alt="Phản hồi dạng ảnh"/>

<div id="phan-hoi-dang-slide-anh">

**d. Phản hồi dạng slide ảnh**

Phản hồi dạng slide ảnh là phản hồi chứa nút bấm và ảnh.

Ví dụ:

<img src="images/response_carousel.png" alt="Phản hồi dạng slide ảnh"/>

<div id="mot-so-khai-niem-kich-ban">

## 6. Kịch bản

Kịch bản mô phỏng luồng trò chuyện giữa người và chatbot.

Một kịch bản có 2 thành phần là ý định (của người dùng) và phản hồi (của chatbot).

Ví dụ 1:

    - Người dùng có ý định đặt bàn
        - Chatbot phản hồi đặt bàn thành công

<img src="images/example_story.png" alt="Kịch bản 1"/>

Một kịch bản có thể gồm một hoặc nhiều ý định và phản hồi.

Ví dụ 2:

    - Người dùng có ý đinh đặt bàn
        - Chatbot phản hồi đặt bàn thành công
        - Chatbot cảm ơn
    - Người dùng có ý định hủy đặt bàn
        - Chatbot phản hồi hủy thành công
        - Chatbot cảm ơn

<img src="images/example_story_2.png" alt="Kịch bản 2"/>

<div id="xay-dung-chatbot">

# C. Xây dựng chatbot trên nền tảng CMC Chatbot Admin

<div id="create-chatbot">

## 1. Tạo chatbot
Tại màn hình quản trị, click "Thêm dự án" để tạo chatbot.

![Tạo chatbot](videos/create_chatbot.mp4)

<div id="build-nlu">

## 2. Hiểu ngôn ngữ tự nhiên - NLU

Hiểu ngôn ngữ tự nhiên là một bước giúp chatbot hiểu được câu nói của người dùng. Các thành phần cơ bản trong bước Hiểu ngôn ngữ tự nhiên trên nền tảng CMC Chatbot Admin:

[Ý định và câu mẫu](#nlu-y-dinh)

[Thực thể](#nlu-thuc-the)

[Từ tương đương](#nlu-tu-tuong-duong)

[Biểu thức chính quy](#nlu-bieu-thuc-chinh-quy)

[Pipeline xử lý ngôn ngữ tự nhiên](#nlu-pipeline)

[Thống kê](#nlu-thong-ke)

<div id="nlu-y-dinh">

**a. Ý định và câu mẫu**

Một câu mẫu luôn và chỉ đi kèm với một ý định.

Tạo câu mẫu và gán ý định cho câu mẫu.

![Tạo ý định và câu mẫu](videos/nlu_create_intent.mp4)

Click vào các biểu tượng bên phải mỗi câu mẫu để chỉnh sửa hoặc xóa câu mẫu. Để chỉnh sửa ý định, click vào ý định muốn chỉnh sửa và chọn hoặc tạo ý định mới.

![Chỉnh sửa ý định và câu mẫu](videos/nlu_edit_intent.mp4)

*Để huấn luyện được, một ý định hoặc một thực thể cần ít nhất 2 câu mẫu.*

<div id="nlu-thuc-the>

**b. Thực thể**

Để tạo thực thể cho câu mẫu, bôi đen một từ/cụm từ và gán một thực thể cho từ/cụm từ đó.

Một câu mẫu có thể chứa nhiều thực thể. Một từ/cụm từ trong một câu mẫu chỉ thuộc về một thực thể.

Ví dụ tạo 2 thực thể *số lượng người* và *thời gian* có giá trị tương ứng là *5* và *8 giờ tối nay*.

![Tạo thực thể](videos/nlu_create_entity.mp4)

<div id="nlu-tu-tuong-duong">

**c. Từ tương đương**

Từ tương đương là những từ có nghĩa tương đương nhau. Bạn có thể sử dụng từ tương đương khi có nhiều cách gọi cho một từ, chẳng hạn như *Hà Nội* và *thủ đô*. Việc sử dụng từ tương đương nhằm đảm bảo chatbot hiểu các từ đó có cùng nghĩa.

Video dưới đây thực hiện tạo các từ tương đương cho từ *Hà Nội*. Các từ tương đương được ngăn cách nhau bởi dấu phẩy.

![Tạo từ tương đương](videos/nlu_create_synonym.mp4)

<div id="nlu-bieu-thuc-chinh-quy">

**d. Biểu thức chính quy**

Biểu thức chính quy là một chuỗi ký tự mô tả quy tắc của từ/cụm từ. Ví dụ giá trị của thực thể *email* được mô tả là bất kỳ từ nào kết thúc bằng *@gmail.com*, khi đó biểu thức chính quy có dạng *r".+@gmail\\.com$"*.

Video dưới đây thực hiện tạo biểu thức chính quy cho thực thể *email*.

![Tạo biểu thức chính quy](videos/nlu_create_regex.mp4)

<div id="nlu-pipeline">

**e. NLU Pipeline**

<div id="nlu-thong-ke">

**f. Thống kê**

<img src="images/nlu_statistic.png" alt="Màn hình thống kê" width="500"/>

Tại màn hình thống kê, bạn có thể xem số lượng các câu mẫu, ý đinh, thực thể, từ tương đương và kịch bản đã tạo. Đồng thời, bạn có thể xuất dữ liệu thống kê số lượng câu mẫu và ý định ra file .csv.

<div id="build-story">

## 3. Tạo kịch bản

CMC Chatbot Admin cung cấp 2 loại kịch bản, gọi là *rule* và *story*. *Rule* được tạo khi bạn muốn chatbot đưa ra phản hồi chính xác như những gì đã được học, tuân theo quy tắc *Nếu ý định của người dùng là I thì chatbot phản hồi nội dung C*. Trong khi đó, *story* được tạo khi bạn muốn chatbot kết hợp các nội dung trò chuyện trước đó để đưa ra phản hồi phù hợp.

Các bước tạo một rule cơ bản được mô tả trong video dưới đây.

![Tạo rule](videos/story_create_rule.mp4)

Các bước tạo một story cơ bản được mô tả trong video dưới đây.

![Tạo story](videos/story_create_story.mp4)

<div id="build-response">

## 4. Tạo phản hồi

### a. Tạo phản hồi dạng văn bản

Để tạo phản hồi dạng văn bản, tại màn hình của story hoặc rule, click *Bot*, sau đó chọn *Nội dung* và nhập nội dung muốn phản hồi vào ô *Nhập một tin nhắn*. Bạn có thể đặt tên cho phản hồi bằng cách chỉnh sửa tên mặc định ở góc dưới bên phải của phản hồi. Để xóa phản hồi, click biểu tượng xóa và xác nhận hoặc hủy bỏ xóa.

Các bước được mô tả trong video dưới đây.

![Tạo phản hồi dạng văn bản](videos/story_create_response_text.mp4)

Để sao chép nội dung một phản hồi, click *Hành động*, sau đó nhập tên của phản hồi muốn sao chép.

![Sao chép phản hồi](videos/story_copy_response.mp4)

<div id="create-response-button">

### b. Tạo phản hồi dạng nút bấm

Để tạo phản hồi dạng nút bấm, tại màn hình của story hoặc rule, click *Bot*, sau đó chọn *Nút bấm* và nhập nội dung cho nút bấm. Các cách đặt tên, sao chép và xóa phản hồi được thực hiện tương tự như phản hồi dạng văn bản.

Một phản hồi dạng nút bấm gồm các nội dung như sau.

<img src="images/story_button_notation_2.png" alt="Phản hồi dạng nút bấm"/>
<img src="images/story_button_notation_3.png" alt="Phản hồi dạng nút bấm"/>

![Tạo phản hồi dạng nút bấm](videos/story_create_response_button.mp4)

<div id="create-response-image">

### c. Tạo phản hồi dạng ảnh

Để tạo phản hồi dạng ảnh, tại màn hình của story hoặc rule, click *Bot*, sau đó chọn *Ảnh* và nhập đường dẫn công khai của ảnh hoặc tải ảnh lên.

![Tạo phản hồi dạng ảnh](videos/story_create_response_image.mp4)

### d. Tạo phản hồi dạng slide ảnh

Để tạo phản hồi dạng slide ảnh, tại màn hình của story hoặc rule, click *Bot*, sau đó chọn *Carousel* (*Slide ảnh*).

*Slide ảnh* có giao diện như sau:

<img src="images/story_carousel_notation_1.png" alt="Giao diện phản hồi dạng slide ảnh"/>

Cách tạo *nút bấm*, *ảnh* tương tự như cách tạo nút bấm, ảnh ở phần [Tạo phản hồi dạng nút bấm](#create-response-button) và [Tạo phản hồi dạng ảnh](#create-response-image).

![Tạo phản hồi dạng slide ảnh](videos/story_create_response_carousel.mp4)

<div id="build-slot">

## 5. Tạo biến

Biến là bộ nhớ tạm của chatbot ([Biến](#mot-so-khai-niem-bien)) và nhận các kiểu giá trị: 

- bool
- categorical
- float
- list
- text
- any

Tạo một biến tên *so_luong* có kiểu *float* được thao tác như sau.

![Tạo biến](videos/story_create_slot.mp4)

<div id="build-form">

## 6. Tạo form

Form là biểu mẫu được sử dụng để thu thập thông tin của người dùng ([Form](#mot-so-khai-niem-form))

Ví dụ một kịch bản đặt bàn có nội dung như sau:

    - Người dùng đặt bàn
      - Chatbot thu thập thông tin về số lượng người và thời gian đặt bàn
      - Chatbot xác nhận yêu cầu đặt bàn

Khi đó, tạo một form thu thập thông tin về số lượng người, thời gian đặt bàn và lưu thông tin vào 2 biến *so_luong* và *thoi_gian* được thực hiện như sau:

![Tạo form](videos/story_create_form.mp4)

Sử dụng form đã tạo trong kịch bản đặt bàn, thông tin được lưu trong biến có thể được hiển thị bằng cách gõ *{{tên biến}}*:

![Sử dụng form](videos/story_use_form.mp4)

Định nghĩa phương thức thu thập thông tin trong form:

![Thu thập thông tin trong form](videos/story_form_assign_slot.mp4)

<div id="link-story">

## 7. Liên kết kịch bản
Liên kết kịch bản là việc nối 2 kịch bản với nhau. Liên kết kịch bản chỉ được thực hiện với dạng *story*.

Ví dụ liên kết 2 kịch bản *chào* và *menu* được thực hiện như sau:

![Liên kết kịch bản](videos/link_story.mp4)

<div id="branch-story">

## 8. Rẽ nhánh kịch bản

Rẽ nhánh kịch bản là việc thực hiện điều kiện *Nếu ... thì ...* trong kịch bản.

Ví dụ tạo một kịch bản rẽ nhánh như sau:

![Rẽ nhánh kịch bản](videos/branch_story.mp4)

<div id="quan-ly-chatbot">

# D. Quản lý chatbot trên nền tảng CMC Chatbot Admin

<div id="phan-quyen">

### 1. Phân quyền

Phân quyền là việc gán một quyền sử dụng hệ thống cho một tài khoản. Các quyền được quản lý tại màn *Quản trị* -> *Quyền*. Màn hình hiển thị danh sách các quyền, bạn có thể tạo một quyền mới, chỉnh sửa hoặc xóa một quyền, tuy nhiên không thể xóa các quyền mặc định (là các quyền cơ bản để tạo quyền mới).

Thao tác thêm và xóa một quyền được thực hiện như sau.

![Thêm và xóa quyền](videos/create_role.mp4)

Tạo tài khoản và phân quyền cho tài khoản được thực hiện tại màn *Quản trị* -> *Tài khoản*.

Thao tác tạo tài khoản và phân quyền cho tài khoản được thực hiện như sau.

![Tạo tài khoản](videos/create_account.mp4)

<div id="lich-su">

### 2. Lịch sử

Màn hình *Lịch sử* bao gồm các thông tin về tin nhắn của người dùng, ý định mà chatbot dự đoán (NLU), cuộc hội thoại giữa chatbot và người dùng (Kịch bản).

<img src="images/history_nlu.png" alt="Lịch sử NLU" width="500"/>

<img src="images/history_story.png" alt="Lịch sử NLU" width="500"/>

<div id="phan-tich">

### 3. Phân tích

Các thống kê về lượt truy cập, tương tác; độ dài cuộc trò chuyện giữa chatbot và người dùng; các ý định phổ biến của người dùng ... được hiển thị trong màn *Phân tích*.

Với mỗi loại thống kê, bạn có thể tùy chỉnh thời gian thống kê, định dạng biểu đồ hiển thị và xuất kết quả ra file excel.

![Phân tích](videos/analysis.mp4)

<div id="imcomming-message">

### 4. Incomming message

# F. Tích hợp chatbot trên các nền tảng